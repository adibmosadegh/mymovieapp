package com.example.mymovieapp.navagation

import androidx.compose.runtime.Composable
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.example.mymovieapp.screens.home.HomeScreen
import com.example.mymovieapp.screens.details.DetailsScreen

@Composable
fun MovieNavigation() {
//    navController, NavHost, NavGraph
    val navController = rememberNavController()
    NavHost(navController = navController,
        startDestination = MovieScreens.HomeScreen.name) {
        composable(MovieScreens.HomeScreen.name) {
            HomeScreen(navController = navController)
        }
        composable(MovieScreens.DetailScreen.name) {
            DetailsScreen(navController = navController)
        }
    }
}