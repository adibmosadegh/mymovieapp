package com.example.mymovieapp.screens.home

import android.annotation.SuppressLint
import android.util.Log
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import com.example.mymovieapp.MovieRow
import com.example.mymovieapp.navagation.MovieScreens

@SuppressLint("UnusedMaterial3ScaffoldPaddingParameter")
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun HomeScreen(navController: NavController) {
    Scaffold(
        topBar = {
            TopAppBar(
                title = { Text(text = "Movie App") },
                colors = TopAppBarDefaults.smallTopAppBarColors(containerColor = Color.Green)
            )
        },
        content = {
            Column {
                Spacer(modifier = Modifier.height(65.dp))
                MainContent(navController = navController)
            }
        }
    )
}

@Composable
fun MainContent(
    navController: NavController,
    movieList: List<String> = listOf("300", "Harry Patter", "Life")) {
    Column(modifier = Modifier.padding(12.dp)) {
        LazyColumn(
            modifier = Modifier.fillMaxSize()
        ) {
            items(items = movieList) {
                MovieRow(movie = it) {movie ->
                    navController.navigate(route = MovieScreens.DetailScreen.name)
                }
            }
        }
    }
}